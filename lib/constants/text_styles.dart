import 'package:flutter/material.dart';

TextStyle kTitleTextStyle = TextStyle(
  color: Colors.white,
  fontSize: 26,
  fontWeight: FontWeight.w600,
);

TextStyle kSubtitleTextStyle = TextStyle(
  color: Colors.white,
  fontSize: 12,
  fontWeight: FontWeight.w400,
);

TextStyle kBlackSubtitleTextStyle = TextStyle(
  color: Colors.black,
  fontSize: 16,
);

TextStyle kBlackTitleTextStyle = TextStyle(
  color: Colors.black,
  fontSize: 26,
);
