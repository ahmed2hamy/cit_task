import 'package:flutter/material.dart';

class ActivitiesModel {
  String activityName;
  IconData activityIcon;

  ActivitiesModel({
    this.activityName,
    this.activityIcon,
  });
}
